# People Directory Homework Project
* Contains a simple .NET Core 2.2 Web API with a local Sql Server Express database backend  
* Leverages Dapper ORM for querying  
* Follows the mediator architecture pattern  

This repo is to be **cloned** and developed to provide a working solution. To work with the solution, 
you will need to install [Visual Studio Community 2019](https://visualstudio.microsoft.com/downloads/?utm_medium=microsoft&utm_source=docs.microsoft.com&utm_campaign=inline+link&utm_content=download+vs2019) and [.NET Core SDK 2.2](https://dotnet.microsoft.com/download/dotnet/2.2)

When done with the solution, please upload the repo to your [GitHub](https://github.com/) or [Bitbucket](https://bitbucket.org/) account, make the repo public, and provide a link to the JO Team for review.

#### To setup your local database and begin on the homework:  
1. Clone the repo and navigate to {repoDirectory}/PeopleApi
2. Open the sln file in VS Community 2019  
3. Select 'View' -> 'SQL Server Object Explorer'  
4. Right click 'SQL Server' and click 'Add SQL Server'  
5. Click 'Local', then 'MSSQLLocalDB', then click the 'Connect' button  
6. Expand the local server you just created, right click the 'Databases' folder, and click 'Add New Database'  
7. Enter 'Directory' as the 'Database Name' and click the 'OK' button 
8. Go back to the solution explorer, right click the solution, and click 'Set Startup Projects'
9. Click the multiple startup projects option and set the action for both projects to 'Start'
10. Click 'OK'
11. Click 'Tools', then 'Nuget Package Manager', then 'Manage Nuget Packages for Solution'
12. Click the gear icon next to package source
13. If the nuget.org repo is not present by default, you will need to add it. The source should be https://api.nuget.org/v3/index.json
14. After verifying that the nuget.org repo is present, click OK
15. Start the app. The DataInitializer class seeds data on startup if none exists.  

##### Technology Used:
[.NET Core MVC Tutorial](https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/razor-pages-start?view=aspnetcore-2.2&tabs=visual-studio)  
[.NET Core API Tutorial](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-2.2&tabs=visual-studio)  
[MediatR Tutorial](https://github.com/jbogard/MediatR)  
[Dapper Tutorial](https://dapper-tutorial.net)  